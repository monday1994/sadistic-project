import React, { Component } from 'react';

import './box-nav.css';

export default class BoxNav extends Component {
    render() {
        return (
            <section className='box-nav'>
                <nav className='box-nav-left'>
                    <a href='#1' className='button'>Add topic</a>
                    <span> | </span>
                    <a href='#1'>Recently popular</a> 
                    <span> | </span>
                    <a href='#1'>Random</a> 
                    <span> | </span>
                    <a href='#1'>Day by day</a> |
                </nav>
                <nav className='box-nav-right'>
                    <a href='#1'>Random</a>
                    <span> • </span>
                    <a href='#1'>Day by day</a>
                </nav>
            </section>
            
        )
    }
}