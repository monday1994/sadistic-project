import React, { Component } from 'react';

import logo from '../../images/logo.webp';
import './header.css';

export default class Header extends Component {
    render() {
        return (
            <header className='header'>
                <div className='logo'>
                    <img src={logo} alt='logo'/>
                </div>
            </header>
        )
    }
}