import React, { Component } from 'react';

import TopicNav from '../TopicNav/TopicNav';
import TopicContent from '../TopicContent/TopicContent';

import './topic.css';

export default class Topic extends Component {
    render() {
        return (
            <section className='topic'>
                <TopicNav />
                <TopicContent />
            </section>
        )
    }
}