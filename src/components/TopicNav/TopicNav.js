import React, { Component } from 'react';

import './topic-nav.css';
import userImage from '../../images/default-user-img.png';
import beerImage from '../../images/beer.webp'

export default class TopicNav extends Component {
    render() {
        return (
            <nav className='topic-nav'>
                <img src={userImage} alt='user-image' />
                
                <div className='topic-nav-left'>
                    <div className='topic-nav-left-content'>
                        <div className='topic-nav-left-title'>
                            <a href='#222'>Awsome title</a>
                        </div>
                        <div className='topic-nav-left-info'>
                            <span>Username</span>
                            <span>22:03</span>
                            <a href='#'>Copy link</a>
                        </div>         
                    </div>                               
                </div>
                <div className='topic-nav-right'>
                    <img src={beerImage} alt='beer-icon' />
                    <div className='topic-nav-right-num-of-beers'>
                        <span>50</span>
                    </div>
                </div>
                
               
            
            </nav>
        )
    }
}