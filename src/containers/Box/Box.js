import React, { Component } from 'react';

import BoxNav from '../../components/BoxNav/BoxNav';
import Topic from '../../components/Topic/Topic';
import './box.css';

export default class Box extends Component {
    render() {
        return (
            <section className='box'>
                <BoxNav />
                <Topic />
            </section>
        )
    }
}