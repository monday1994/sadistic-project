import React, { Component } from 'react';
import Box from '../Box/Box';
import TopNav from '../../components/TopNav/TopNav';

import './main.css';

export default class Main extends Component {
    render() {
        return (
            <main className='content'>
                <TopNav />
                <Box />
            </main>
        )
    }
}